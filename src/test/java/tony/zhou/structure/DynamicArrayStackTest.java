package tony.zhou.structure;

import org.junit.Test;
import tony.zhou.structure.stack.DynamicArrayStack;

import static org.junit.Assert.assertEquals;

public class DynamicArrayStackTest {

	@Test
	public void push_oneItem_itemOnTopOfStack() {
		DynamicArrayStack stack = new DynamicArrayStack();

		String item = "anItem";
		stack.push(item);

		assertEquals(item, stack.peek());
	}

	@Test
	public void push_moreItemsThanInitialSize_stackGrowDynamically() {
		DynamicArrayStack stack = new DynamicArrayStack(1);

		String item1 = "item1";
		stack.push(item1);
		String item2 = "item2";
		stack.push(item2);
		String item3 = "item3";
		stack.push(item3);

		assertEquals(item3, stack.peek());
	}

	@Test
	public void top_newStack_null() {
		DynamicArrayStack stack = new DynamicArrayStack();

		assertEquals(null, stack.peek());
	}

	@Test
	public void pop_newStack_null() {
		DynamicArrayStack stack = new DynamicArrayStack(1);

		assertEquals(null, stack.pop());
	}

	@Test
	public void pop_oneItemFromStack() {
		DynamicArrayStack stack = new DynamicArrayStack(1);

		String item1 = "item1";
		stack.push(item1);
		String item2 = "item2";
		stack.push(item2);

		assertEquals(item2, stack.pop());
	}

}
