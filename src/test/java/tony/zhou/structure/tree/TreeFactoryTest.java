package tony.zhou.structure.tree;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class TreeFactoryTest {

	@Test
	public void make_fromInorderAndLevelTraversal_returnTree() {
		TreeFactory<Integer> factory = new TreeFactory<>();
		final InOrder<Integer> inOrder = new InOrder<Integer>(Arrays.asList(1,
				2, 4, 6, 8, 9, 10));
		final LevelOrder<Integer> levelOrder = new LevelOrder<Integer>(
				Arrays.asList(8, 4, 9, 2, 6, 10, 1));

		Node<Integer> root = factory.make(inOrder, levelOrder);

		assertEquals(Integer.valueOf(8), root.getValue());
		assertEquals(Integer.valueOf(4), root.getLeft().getValue());
		assertEquals(Integer.valueOf(2), root.getLeft().getLeft().getValue());
		assertEquals(Integer.valueOf(1), root.getLeft().getLeft().getLeft()
				.getValue());
		assertEquals(Integer.valueOf(6), root.getLeft().getRight().getValue());
		assertEquals(Integer.valueOf(9), root.getRight().getValue());
		assertEquals(Integer.valueOf(10), root.getRight().getRight().getValue());
	}
}
