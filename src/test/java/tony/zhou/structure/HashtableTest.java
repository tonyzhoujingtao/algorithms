package tony.zhou.structure;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class HashtableTest {

	@Test
	public void put_keyValuePair_retriveValueByKey() {
		Hashtable<String, String> hashtable = new Hashtable<>(10);

		String key = "name";
		String value = "tony";
		hashtable.put(key, value);

		assertEquals(value, hashtable.get(key));
	}

	@Test
	public void put_keyValuePair_increaseSizeOfTableByOne() {
		Hashtable<String, String> hashtable = new Hashtable<>(10);

		String key = "name";
		String value = "tony";
		hashtable.put(key, value);

		assertEquals(1, hashtable.size());
	}

	@Test
	public void put_keyValuePairWithExistingKey_replaceTheExistingKeyValuePair() {
		Hashtable<String, String> hashtable = new Hashtable<>(10);

		String key = "name";
		String oldValue = "zhou";
		hashtable.put(key, oldValue);
		String value = "tony";
		hashtable.put(key, value);

		assertEquals(value, hashtable.get(key));
	}

	@Test
	public void put_keyValuePairWithExistingKey_remainSameSize() {
		Hashtable<String, String> hashtable = new Hashtable<>(10);

		String key = "name";
		String oldValue = "zhou";
		hashtable.put(key, oldValue);
		String value = "tony";
		hashtable.put(key, value);

		assertEquals(1, hashtable.size());
	}

	@Test
	public void remove_existingKey_deleteEntry() {
		Hashtable<String, String> hashtable = new Hashtable<>(10);

		String key = "name";
		String value = "tony";
		hashtable.put(key, value);
		hashtable.remove(key);

		assertEquals(0, hashtable.size());
		assertEquals(null, hashtable.get(key));
	}

	@Test
	public void remove_nonExistingKey_preserveOldState() {
		Hashtable<String, String> hashtable = new Hashtable<>(10);

		String key = "name";
		String value = "tony";
		hashtable.put(key, value);
		String newKey = "age";
		hashtable.remove(newKey);

		assertEquals(1, hashtable.size());
		assertEquals(value, hashtable.get(key));
	}

	@Test
	public void put_moreKeyValuePairsThanInitTableSize_hashTableGrows() {
		Hashtable<String, String> hashtable = new Hashtable<>(3);

		String firstKey = "name";
		String firstValue = "tony";
		hashtable.put(firstKey, firstValue);
		hashtable.put("age", "unknown");
		hashtable.put("interest", "programming");
		hashtable.put("language", "english");
		hashtable.put("race", "chinese");
		String lastKey = "gender";
		String lastValue = "male";
		hashtable.put(lastKey, lastValue);

		assertEquals(6, hashtable.size());
		assertEquals(firstValue, hashtable.get(firstKey));
		assertEquals(lastValue, hashtable.get(lastKey));
	}
}
