package tony.zhou.algorithm.sort;

import java.util.Arrays;
import java.util.Random;

public class QuickSort {

	public static void main(String[] args) {
		int[] array = { 10, 2, 11, 20, 38, 4, 93, 12, 44, 57, 3, 29, 75 };

		int[] sortedArray = quickSort(array);

		System.out.println(Arrays.toString(sortedArray));
	}

	private static int[] quickSort(int[] array) {
		return quickSort(array, 0, array.length - 1);
	}

	private static int[] quickSort(int[] array, int start, int end) {
		if (start < end) {
			int pivotIndex = randomPartition(array, start, end);
			quickSort(array, start, pivotIndex - 1);
			quickSort(array, pivotIndex + 1, end);
		}
		return array;
	}

	private static int randomPartition(int[] array, int start, int end) {
		Random r = new Random();
		int pivotIndex = start + r.nextInt(end - start);
		swap(array, pivotIndex, end);

		return partition(array, start, end);
	}

	private static int partition(int[] array, int start, int end) {
		int pivotElement = array[end];
		int pivotIndex = start;
		for (int j = start; j <= end - 1; ++j) {
			if (array[j] <= pivotElement) {
				swap(array, pivotIndex, j);
				++pivotIndex;
			}
		}

		swap(array, pivotIndex, end);

		return pivotIndex;
	}

	private static void swap(int[] array, int i, int j) {
		int tmp = array[i];
		array[i] = array[j];
		array[j] = tmp;
	}

}
