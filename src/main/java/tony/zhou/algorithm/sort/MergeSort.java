package tony.zhou.algorithm.sort;

import java.util.Arrays;

public class MergeSort {

    public static void main(String[] args) {
        int[] array = {10, 2, 11, 20, 38, 4, 93, 12, 44, 57, 3, 29, 75};

        topDownMergeSort(array);

        System.out.println(Arrays.toString(array));
    }

    private static void topDownMergeSort(int array[]) {
        int[] tmpArray = new int[array.length];
        int n = array.length;
        topDownSplitMerge(array, 0, n, tmpArray);
    }

    private static void topDownSplitMerge(int array[], int beginIndex,
                                          int endIndex, int tmpArray[]) {
        if (endIndex - beginIndex < 2)
            return;

        int iMiddle = (endIndex + beginIndex) / 2;
        topDownSplitMerge(array, beginIndex, iMiddle, tmpArray);
        topDownSplitMerge(array, iMiddle, endIndex, tmpArray);
        topDownMerge(array, beginIndex, iMiddle, endIndex, tmpArray);
        copyArray(tmpArray, beginIndex, endIndex, array);
    }

    private static void topDownMerge(int array[], int beginIndex,
                                     int middleIndex, int endIndex, int tmpArray[]) {
        int i0 = beginIndex, i1 = middleIndex;

        for (int j = beginIndex; j < endIndex; j++) {
            if (i0 < middleIndex && (i1 >= endIndex || array[i0] <= array[i1])) {
                tmpArray[j] = array[i0];
                i0 = i0 + 1;
            } else {
                tmpArray[j] = array[i1];
                i1 = i1 + 1;
            }
        }
    }

    private static void copyArray(int tmpArray[], int beginIndex, int endIndex,
                                  int array[]) {
        for (int k = beginIndex; k < endIndex; k++)
            array[k] = tmpArray[k];
    }

}
