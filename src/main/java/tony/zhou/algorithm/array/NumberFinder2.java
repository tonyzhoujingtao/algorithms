package tony.zhou.algorithm.array;

/**
 * http://www.careercup.com/question?id=12908676
 */
public class NumberFinder2 {
    public static void main(String[] args) {
        NumberFinder2 finder = new NumberFinder2();

        System.out.println(finder.find(new int[]{10, 12, 56, 1, 3, 8}, 3));
        System.out.println(finder.find(new int[]{10, 12, 56, 1, 3, 8}, 8));
        System.out.println(finder.find(new int[]{10, 12, 56, 1, 3, 8}, 10));
        System.out.println(finder.find(new int[]{10, 12, 56, 1, 3, 8}, 1));
        System.out.println(finder.find(new int[]{10, 12, 56, 1, 3, 8}, 56));

        System.out.println();

        System.out.println(finder.find(new int[]{10, 12, 56, 1, 3, 4, 5, 8}, 10));
        System.out.println(finder.find(new int[]{10, 12, 56, 1, 3, 4, 5, 8}, 56));
        System.out.println(finder.find(new int[]{10, 12, 56, 1, 3, 4, 5, 8}, 1));
        System.out.println(finder.find(new int[]{10, 12, 56, 1, 3, 4, 5, 8}, 8));

        System.out.println();

        System.out.println(finder.find(new int[]{4, 5, 6, 7, 0, 1, 2}, 4));
        System.out.println(finder.find(new int[]{4, 5, 6, 7, 0, 1, 2}, 7));
        System.out.println(finder.find(new int[]{4, 5, 6, 7, 0, 1, 2}, 0));
        System.out.println(finder.find(new int[]{4, 5, 6, 7, 0, 1, 2}, 2));
    }

    public int find(int[] numbers, int target) {
        if (numbers != null && numbers.length != 0) {
            int l = 0;
            int h = numbers.length - 1;

            while (l <= h) {
                int m = l + (h - l) / 2;
                if (numbers[m] == target) {
                    return m;
                }

                if (numbers[l] < numbers[m]) {
                    if (numbers[l] <= target && target < numbers[m]) {
                        h = m - 1;
                    } else {
                        l = m + 1;
                    }
                } else {
                    if (numbers[m] < target && target <= numbers[h]) {
                        l = m + 1;
                    } else {
                        h = m - 1;
                    }
                }
            }
        }

        return -1;
    }
}
