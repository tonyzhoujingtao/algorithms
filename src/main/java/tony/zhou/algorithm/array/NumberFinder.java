package tony.zhou.algorithm.array;

/**
 * http://www.careercup.com/question?id=12908676
 */
public class NumberFinder {
    public static void main(String[] args) {
        NumberFinder finder = new NumberFinder();

        System.out.println(finder.find(new int[]{10, 12, 56, 1, 3, 8}, 3));
        System.out.println(finder.find(new int[]{10, 12, 56, 1, 3, 8}, 8));
        System.out.println(finder.find(new int[]{10, 12, 56, 1, 3, 8}, 10));
        System.out.println(finder.find(new int[]{10, 12, 56, 1, 3, 8}, 1));
        System.out.println(finder.find(new int[]{10, 12, 56, 1, 3, 8}, 56));

        System.out.println();

        System.out.println(finder.find(new int[]{10, 12, 56, 1, 3, 4, 5, 8}, 10));
        System.out.println(finder.find(new int[]{10, 12, 56, 1, 3, 4, 5, 8}, 56));
        System.out.println(finder.find(new int[]{10, 12, 56, 1, 3, 4, 5, 8}, 1));
        System.out.println(finder.find(new int[]{10, 12, 56, 1, 3, 4, 5, 8}, 8));

        System.out.println();

        System.out.println(finder.find(new int[]{4, 5, 6, 7, 0, 1, 2}, 4));
        System.out.println(finder.find(new int[]{4, 5, 6, 7, 0, 1, 2}, 7));
        System.out.println(finder.find(new int[]{4, 5, 6, 7, 0, 1, 2}, 0));
        System.out.println(finder.find(new int[]{4, 5, 6, 7, 0, 1, 2}, 2));

        System.out.println();

        System.out.println(finder.find(new int[]{1, 2, 1, 1, 1, 1}, 2));
    }

    public int find(int[] numbers, int target) {
        if (numbers != null && numbers.length != 0) {
            int division = findDivision(numbers);

            if (target <= numbers[numbers.length - 1] && target >= numbers[division]) {
                return binarySearch(numbers, target, division, numbers.length - 1);
            }

            if (target <= numbers[division - 1] && target >= numbers[0]) {
                return binarySearch(numbers, target, 0, division - 1);
            }
        }

        return -1;
    }

    private int findDivision(int[] numbers) {
        int l = 0;
        int h = numbers.length - 1;

        while (l <= h) {
            int m = (l + h) / 2;

            if (numbers[l] > numbers[m]) {
                h = m - 1;

                if (numbers[l] <= numbers[h]) {
                    return h + 1;
                }
            } else if (numbers[h] < numbers[m]) {
                l = m + 1;

                if (numbers[l] <= numbers[h]) {
                    return l;
                }
            }
        }

        return -1;
    }

    private int binarySearch(int[] numbers, int target, int l, int h) {
        while (l <= h) {
            int m = (l + h) / 2;

            if (numbers[m] == target) {
                return m;
            }
            if (numbers[m] < target) {
                l = m + 1;
            } else {
                h = m - 1;
            }
        }

        return -1;
    }
}
