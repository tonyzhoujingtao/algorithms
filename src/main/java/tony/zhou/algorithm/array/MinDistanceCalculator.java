package tony.zhou.algorithm.array;

import java.util.ArrayList;
import java.util.List;

/**
 * Given a string and two words which are present in the string, find the minimum distance between the words
 * Eg: "the brown qucik frog quick the", "the" "quick" O/P -> 1
 * "the quick the brown quick brown the frog", "the" "the" O/P -> 2
 */
public class MinDistanceCalculator {
    public static void main(String[] args) {
        MinDistanceCalculator c = new MinDistanceCalculator();
        System.out.println(c.calculate("the brown qucik frog quick the", "the", "quick"));
        System.out.println(c.calculate("the quick the brown quick brown the frog", "the", "the"));
    }

    public int calculate(String string, String w1, String w2) {
        List<Integer> index1 = new ArrayList<>();
        List<Integer> index2 = new ArrayList<>();
        String[] words = string.trim().split("\\s+");
        for (int i = 0; i < words.length; i++) {
            if (words[i].equals(w1)) {
                index1.add(i);
            }
            if (words[i].equals(w2)) {
                index2.add(i);
            }
        }

        if (index1.isEmpty()) {
            throw new IllegalArgumentException("'" + w1 + "' not found in string");
        }

        if (index2.isEmpty()) {
            throw new IllegalArgumentException("'" + w2 + "' not found in string");
        }

        int min = Integer.MAX_VALUE;
        int p1 = 0, p2 = 0;
        while (p1 < index1.size() && p2 < index2.size()) {
            int diff = index1.get(p1) - index2.get(p2);
            int distance = Math.abs(diff);

            if (distance != 0) {
                min = Math.min(min, distance);
            }

            if (diff < 0) {
                ++p1;
            } else {
                ++p2;
            }
        }

        return min;
    }
}
