package tony.zhou.algorithm.array;

import java.util.HashSet;
import java.util.Set;

/**
 * A zero-indexed array A consisting of N different integers is given. The array contains integers in the range [1..(N + 1)], which means that exactly one element is missing.
 * <p/>
 * Your goal is to find that missing element.
 * <p/>
 * Write a function:
 * <p/>
 * class Solution { public int solution(int[] A); }
 * <p/>
 * that, given a zero-indexed array A, returns the value of the missing element.
 * <p/>
 * For example, given array A such that:
 * <p/>
 * A[0] = 2
 * A[1] = 3
 * A[2] = 1
 * A[3] = 5
 * the function should return 4, as it is the missing element.
 * <p/>
 * Assume that:
 * <p/>
 * N is an integer within the range [0..100,000];
 * the elements of A are all distinct;
 * each element of array A is an integer within the range [1..(N + 1)].
 * Complexity:
 * <p/>
 * expected worst-case time complexity is O(N);
 * expected worst-case space complexity is O(1), beyond input storage (not counting the storage required for input arguments).
 * Elements of input arrays can be modified.
 */
public class PermMissingElem {
    public static void main(String[] args) {
        System.out.println(solution2(new int[]{2, 3, 1, 5}));
    }

    public static int solution2(int[] array) {
        int sum = (array.length * (array.length + 1)) / 2;

        for (int v : array) {
            sum -= v;
        }

        return sum + array.length + 1;
    }

    public int solution(int[] array) {
        Set<Integer> expected = new HashSet<>();
        for (int i = 0; i < array.length; i++) {
            expected.add(i + 1);
        }

        for (int v : array) {
            expected.remove(v);
        }

        return expected.toArray(new Integer[1])[0];
    }
}
