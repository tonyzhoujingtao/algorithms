package tony.zhou.algorithm.string;

public class Splitter {
    public static void main(String[] args) {
        String[] strings = " hello    world        from\ntony ".trim().split(("\\s+"));
        for (String string : strings) {
            System.out.println(string);
        }
    }
}
