package tony.zhou.algorithm.recursive;

/**
 * Write a method to generate the nth Fibonacci number.
 */
public class Fibonnaci {
    private static long[] fib = new long[100];

    public static void main(String[] args) {
        System.out.println(fib(40));
    }

    public static long fib(int n) {
        if (n <= 0) return 0;
        if (n == 1) return 1;
        if (n == 2) return 1;

        if (fib[n] == 0) {
            fib[n] = fib(n - 1) + fib(n - 2);
        }
        return fib[n];
    }
}
