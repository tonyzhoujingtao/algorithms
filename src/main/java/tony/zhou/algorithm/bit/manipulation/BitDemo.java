package tony.zhou.algorithm.bit.manipulation;

public class BitDemo {
    public static void main(String[] args) {
        int b1 = 0xFFFF;
        int b2 = 0x2002;

        System.out.println(b1 & b2);
        System.out.println(Math.pow(2, 12) * 2 + 2);

        b1 = 0x00FF;
        b2 = 0x0019;
        System.out.println(b2);
        System.out.println(b2 << 2);
        System.out.println((b2 << 2) & b1);
        System.out.println(b2 << 4);
        System.out.println((b2 << 4) & b1);
        System.out.println(Math.pow(2, 7) + Math.pow(2, 4));

        System.out.println(1 << 2);
        System.out.println(Integer.parseInt("100", 2));
        System.out.println(~0);
        System.out.println(Integer.toBinaryString(4));
        System.out.println(Integer.toBinaryString(~0));
    }
}
