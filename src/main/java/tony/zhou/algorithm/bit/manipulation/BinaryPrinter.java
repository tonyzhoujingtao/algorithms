package tony.zhou.algorithm.bit.manipulation;

/**
 * Given a (decimal - e.g. 3.72) number that is passed in as a string, print the binary rep- resentation.
 * If the number can not be represented accurately in binary, print “ERROR”
 */
public class BinaryPrinter {
    public static void main(String[] args) {
        System.out.println(print("3.72"));
        System.out.println(print("3.5"));
        System.out.println(print("3.25"));
        System.out.println(print("3.75"));
        System.out.println(print("3.3"));
    }

    public static String print(String decimal) {
        int integer = Integer.parseInt(decimal.substring(0, decimal.indexOf('.')));
        double fraction = Double.parseDouble(decimal.substring(decimal.indexOf('.')));

        String integerBinary = Integer.toBinaryString(integer);

        StringBuilder fractionBinary = new StringBuilder();
        while (fraction > 0) {
            if (fractionBinary.length() > 32) return "ERROR";
            if (fraction == 1) {
                fractionBinary.append("1");
                break;
            }

            fraction *= 2;

            if (fraction >= 1) {
                fractionBinary.append("1");
                fraction -= 1;
            } else {
                fractionBinary.append("0");
            }
        }

        return integerBinary + "." + fractionBinary.toString();
    }
}
