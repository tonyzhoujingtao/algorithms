package tony.zhou.algorithm.bit.manipulation;

/**
 * You are given two 32-bit numbers, N and M, and two bit positions, i and j. Write a method to set all bits between i and j in N equal to M (e.g., M becomes a substring of N located at i and starting at j).
 * EXAMPLE:
 * Input: N = 10000000000, M = 10101, i = 2, j = 6
 * Output: N = 10001010100
 */
public class BigUpdater {
    public static void main(String[] args) {
        int n = 0b10000000000;
        int m = 0b10101;

        System.out.println(Integer.toBinaryString(updateBits(n, m, 2, 6)));
    }

    public static int updateBits(int n, int m, int i, int j) {
        int max = ~0;
        System.out.println(Integer.toBinaryString(max));

        int lowerBits = (1 << i) - 1;
        System.out.println(Integer.toBinaryString(lowerBits));

        int higherBits = (1 << j) - 1;
        System.out.println(Integer.toBinaryString(higherBits));

        System.out.println(Integer.toBinaryString(higherBits - lowerBits));

        int mask = max ^ (higherBits - lowerBits);
        System.out.println(Integer.toBinaryString(mask));

        return (n & mask) | (m << i);
    }
}
