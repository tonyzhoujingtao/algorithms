package tony.zhou.challenge;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by tonyzhou on 27/1/15.
 * <p/>
 * Given an array of integers. Find two disjoint contiguous sub-arrays such that the absolute difference between the sum of two sub-array is maximum.
 * The sub-arrays should not overlap.
 * <p/>
 * eg- [2 -1 -2 1 -4 2 8] ans - (-1 -2 1 -4) (2 8), diff = 16
 */
public class DifferenceMaximizer {
    private final List<Integer> integers;
    private final List<Integer> minStart;
    private final List<Integer> maxStart;
    private final List<Integer> minEnd;
    private final List<Integer> maxEnd;

    private final List<Integer> minStartRange;
    private final List<Integer> maxStartRange;
    private final List<Integer> minEndRange;
    private final List<Integer> maxEndRange;

    private int maxDifference;

    public DifferenceMaximizer(List<Integer> integers) {
        this.integers = new ArrayList<>(integers);

        minStart = new ArrayList<>(integers.size());
        maxStart = new ArrayList<>(integers.size());
        minEnd = new ArrayList<>(integers.size());
        maxEnd = new ArrayList<>(integers.size());

        minStartRange = new ArrayList<>(integers.size());
        maxStartRange = new ArrayList<>(integers.size());
        minEndRange = new ArrayList<>(integers.size());
        maxEndRange = new ArrayList<>(integers.size());

        initialize(minStart);
        initialize(maxStart);
        initialize(minEnd);
        initialize(maxEnd);

        initialize(minStartRange);
        initialize(maxStartRange);
        initialize(minEndRange);
        initialize(maxEndRange);

        maxDifference = -1;
    }

    private void initialize(List<Integer> list) {
        for (int i = 0; i < integers.size(); i++) {
            list.add(0);
        }
    }

    public void maximize() {
        calculateMinStart();
        calculateMaxStart();
        calculateMinEnd();
        calculateMaxEnd();

        calculateMinStartRange();
        calculateMaxStartRange();
        calculateMinEndRange();
        calculateMaxEndRange();

        calculateLeftMinRightMaxScenarios();
        calculateLeftMaxRightMinScenarios();
    }

    private void calculateLeftMaxRightMinScenarios() {
        for (int i = 0; i < integers.size() - 1; i++) {
            int tmpMaxDifference = Math.abs(maxEndRange.get(i) - minStartRange.get(i + 1));

            if (maxDifference < tmpMaxDifference) {
                maxDifference = tmpMaxDifference;
            }
        }
    }

    private void calculateLeftMinRightMaxScenarios() {
        for (int i = 0; i < integers.size() - 1; i++) {
            int tmpMaxDifference = Math.abs(minEndRange.get(i) - maxStartRange.get(i + 1));

            if (maxDifference < tmpMaxDifference) {
                maxDifference = tmpMaxDifference;
            }
        }
    }

    private void calculateMaxEndRange() {
        final int startIndex = 0;
        maxEndRange.set(startIndex, maxEnd.get(startIndex));

        for (int i = startIndex + 1; i < maxEnd.size(); ++i) {
            maxEndRange.set(i, Math.max(maxEnd.get(i), maxEndRange.get(i - 1)));
        }
    }

    private void calculateMinEndRange() {
        final int startIndex = 0;
        minEndRange.set(startIndex, minEnd.get(startIndex));

        for (int i = startIndex + 1; i < minEnd.size(); ++i) {
            minEndRange.set(i, Math.min(minEnd.get(i), minEndRange.get(i - 1)));
        }
    }

    private void calculateMaxStartRange() {
        final int endIndex = maxStart.size() - 1;
        maxStartRange.set(endIndex, maxStart.get(endIndex));

        for (int i = endIndex - 1; i >= 0; --i) {
            maxStartRange.set(i, Math.max(maxStart.get(i), maxStartRange.get(i + 1)));
        }
    }

    private void calculateMinStartRange() {
        final int endIndex = minStart.size() - 1;
        minStartRange.set(endIndex, minStart.get(endIndex));

        for (int i = endIndex - 1; i >= 0; --i) {
            minStartRange.set(i, Math.min(minStart.get(i), minStartRange.get(i + 1)));
        }
    }

    private void calculateMaxEnd() {
        final int startIndex = 0;
        maxEnd.set(startIndex, integers.get(startIndex));

        for (int i = startIndex + 1; i < integers.size(); ++i) {
            maxEnd.set(i, Math.max(integers.get(i), integers.get(i) + maxEnd.get(i - 1)));
        }
    }

    private void calculateMinEnd() {
        final int startIndex = 0;
        minEnd.set(startIndex, integers.get(startIndex));

        for (int i = startIndex + 1; i < integers.size(); ++i) {
            minEnd.set(i, Math.min(integers.get(i), integers.get(i) + minEnd.get(i - 1)));
        }
    }

    private void calculateMaxStart() {
        final int endIndex = integers.size() - 1;
        maxStart.set(endIndex, integers.get(endIndex));

        for (int i = endIndex - 1; i >= 0; --i) {
            maxStart.set(i, Math.max(integers.get(i), integers.get(i) + maxStart.get(i + 1)));
        }
    }

    private void calculateMinStart() {
        final int endIndex = integers.size() - 1;
        minStart.set(endIndex, integers.get(endIndex));

        for (int i = endIndex - 1; i >= 0; --i) {
            minStart.set(i, Math.min(integers.get(i), integers.get(i) + minStart.get(i + 1)));
        }
    }

    public int getMaxDifference() {
        return maxDifference;
    }


    public static void main(String[] args) {
        DifferenceMaximizer maximizer = new DifferenceMaximizer(Arrays.asList(200, 1, 2, 8, 200, 1, 2, 8, 200, 1, 2, 8, 200, 1, 2, 8, -10, -20, -30, -50, -10, -20, -30, -50, -10, -20, -30, -50, -10, -20, -30, -50, 100));

        maximizer.maximize();

        System.out.println(maximizer.getMaxDifference());
    }
}
