package tony.zhou.challenge;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tonyzhou on 1/2/15.
 * <p/>
 * Given two strings a and b, find whether any anagram of string a is a sub-string of string b. For eg:
 * if a = xyz and b = afdgzyxksldfm then the program should return true.
 */
public class AnagramFinder {

    private final List<String> anagram;
    private final String target;

    public AnagramFinder(String anagram, String target) {
        this.anagram = new ArrayList<>();
        this.target = target;

        for (int i = 0; i < anagram.length(); i++) {
            this.anagram.add(String.valueOf(anagram.charAt(i)));
        }
    }

    public static void main(String[] args) {
        AnagramFinder finder = new AnagramFinder("axyz", "afdgzyxksldfm");
        System.out.println(finder.find());
    }

    public boolean find() {
        for (int i = 0; i < target.length() - anagram.size(); i++) {
            String subTarget = target.substring(i, i + anagram.size());

            boolean isAnagram = true;

            for (String character : anagram) {
                isAnagram = subTarget.contains(character) && isAnagram;

                if(!isAnagram){
                    break;
                }
            }

            if(isAnagram){
                return true;
            }
        }

        return false;
    }
}
