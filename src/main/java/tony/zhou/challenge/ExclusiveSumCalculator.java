package tony.zhou.challenge;

/**
 * Created by tonyzhou on 21/1/15.
 * <p/>
 * You are given an array of integers 'a' that can fit in a memory. Write a method that retuns an array of the same lenght such that each element 'i' of this array is a sum of 'a' except the element a[i]. You are not allowed to use '-' operator.
 */
public class ExclusiveSumCalculator {
    private Integer[] sumFromStart;
    private Integer[] sumToEnd;

    public int[] calculate(int array[]) {
        initializeSumFromStart(array);
        initializeSumToEnd(array);

        return calculateExclusiveSum(array);
    }

    private int[] calculateExclusiveSum(int[] array) {
        int[] exclusiveSum = new int[array.length];

        for (int i = 0; i < exclusiveSum.length; i++) {
            exclusiveSum[i] = sumFromStart(i - 1, array) + sumToEnd(i + 1, array);
        }

        return exclusiveSum;
    }

    private void initializeSumToEnd(int[] array) {
        sumToEnd = new Integer[array.length];
        for (int i = 0; i < sumToEnd.length; i++) {
            sumToEnd[i] = null;
        }
    }

    private void initializeSumFromStart(int[] array) {
        sumFromStart = new Integer[array.length];
        for (int i = 0; i < sumFromStart.length; i++) {
            sumFromStart[i] = null;
        }
    }

    private int sumToEnd(int index, int[] array) {
        if (index < 0 || index >= array.length) {
            return 0;
        }

        if (sumToEnd[index] == null) {
            sumToEnd[index] = sumToEnd(index + 1, array) + array[index];
        }

        return sumToEnd[index];
    }

    private int sumFromStart(int index, int[] array) {
        if (index < 0 || index >= array.length) {
            return 0;
        }

        if (sumFromStart[index] == null) {
            sumFromStart[index] = sumFromStart(index - 1, array) + array[index];
        }
        return sumFromStart[index];
    }

    public static void main(String[] args) {
        int[] array = new int[1000];
        for (int i = 0; i < array.length; i++) {
            array[i] = i + 1;
        }

        ExclusiveSumCalculator calculator = new ExclusiveSumCalculator();
        int[] exclusiveSum = calculator.calculate(array);
        for (int i = 0; i < exclusiveSum.length; i++) {
            System.out.println(exclusiveSum[i]);
        }
    }
}
