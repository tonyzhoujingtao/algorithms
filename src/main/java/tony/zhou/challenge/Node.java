package tony.zhou.challenge;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tonyzhou on 21/1/15.
 * <p/>
 * Given two binary trees, A and B,
 * we can define A as a subset of B if the root of A exists in B and if we can superimpose A on B at that location without changing B. (That is, the subtrees from the root of A and the node in B that is the same as the root of A are the same),
 * <p/>
 * Example:
 * <p/>
 * A:
 * <p/>
 * 5
 * 4	7
 * <p/>
 * B:
 * 6
 * 5	12
 * 4	7	8	10
 * <p/>
 * A is a subset of B in this case.
 * <p/>
 * B1:
 * 6
 * 5	12
 * 4	7	8	10
 * 1
 * <p/>
 * A is still a subset of B1, even though B1 extends one more level than A.
 * <p/>
 * Write an algorithm to determine if one tree is a subset of another tree
 */
public class Node {
    private final int value;

    private final Node leftChild;
    private final Node rightChild;

    public Node(int value, Node leftChild, Node rightChild) {
        this.value = value;
        this.leftChild = leftChild;
        this.rightChild = rightChild;
    }


    public boolean isSubSetOf(Node node) {
        List<Node> childNodes = node.findEqualChildNodes(this);

        for (Node childNode : childNodes) {
            isSimilarTo(childNode);
        }

        return false;
    }

    private List<Node> findEqualChildNodes(Node node) {
        List<Node> childNodes = new ArrayList<>();

        if (value == node.value) {
            childNodes.add(this);
        }

        childNodes.addAll(leftChild.findEqualChildNodes(node));
        childNodes.addAll(rightChild.findEqualChildNodes(node));

        return childNodes;
    }

    private boolean isSimilarTo(Node node) {
        return value == node.value && isLeftChildSimilar(node) && isRightChildSimilar(node);
    }

    private boolean isRightChildSimilar(Node node) {
        return (rightChild == null || rightChild.isSimilarTo(node.rightChild));
    }

    private boolean isLeftChildSimilar(Node node) {
        return (leftChild == null || leftChild.isSimilarTo(node.leftChild));
    }
}
