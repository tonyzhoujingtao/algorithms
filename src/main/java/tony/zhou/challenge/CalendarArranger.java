package tony.zhou.challenge;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by tonyzhou on 23/1/15.
 * <p/>
 * Given a set of busy time intervals of two people as in a calendar, find the free time intervals of both the people so as to arrange a new meeting
 * input: increasing sequence of pair of numbers
 * per1: (1,5) (10, 14) (19,20) (21,24) (27,30)
 * per2: (3,5) (12,15) (18, 21) (23, 24)
 * ouput: (6,9) (16,17) (25,26)
 */
public class CalendarArranger {
    List<Interval> arrange(List<Interval> intervals1, List<Interval> intervals2) {
        boolean isBusy[] = new boolean[32];
        for (int i = 0; i < isBusy.length; i++) {
            isBusy[i] = false;
        }

        markBusyFor(intervals1, isBusy);
        markBusyFor(intervals2, isBusy);


        List<Interval> freeIntervals = new ArrayList<>();
        int start = -1, end = -1;
        for (int i = 1; i < isBusy.length; i++) {
            if (!isBusy[i]) {
                if (start < 0) {
                    start = i;
                }
                end = i;
            } else {
                if (end >= 0) {
                    freeIntervals.add(new Interval(start, end));
                }

                start = -1;
                end = -1;
            }
        }

        if (end >= 0) {
            freeIntervals.add(new Interval(start, end));
        }


        return freeIntervals;
    }

    private void markBusyFor(List<Interval> intervals, boolean[] isBusy) {
        for (Interval interval : intervals) {
            for (int i = interval.start; i <= interval.end; i++) {
                isBusy[i] = true;
            }
        }
    }

    private static class Interval {
        private final int start;
        private final int end;

        private Interval(int start, int end) {
            this.start = start;
            this.end = end;
        }

        @Override
        public String toString() {
            return "(" + start + ", " + end + ')';
        }
    }

    public static void main(String[] args) {
        List<Interval> interval1 = Arrays.asList(new Interval(1, 5), new Interval(10, 14), new Interval(19, 20), new Interval(21, 24), new Interval(27, 30));
        List<Interval> interval2 = Arrays.asList(new Interval(3, 5), new Interval(12, 15), new Interval(18, 21), new Interval(23, 24));

        CalendarArranger arranger = new CalendarArranger();
        System.out.println(arranger.arrange(interval1, interval2));
    }
}
