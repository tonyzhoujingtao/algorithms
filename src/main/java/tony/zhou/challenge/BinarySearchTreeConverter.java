package tony.zhou.challenge;

/**
 * Created by tonyzhou on 22/1/15.
 * <p/>
 * Given a sorted doubly linked list, write an algorithm to convert this list into a binary search tree (BST). The BST will be used to represent a set. You may expect that client will use it to search for presence of a key in the set.
 * You may assume that you are given the following Node implementation that you can not exted or modify
 */
public class BinarySearchTreeConverter {

    public BinaryTreeNode convert(LinkedNode node, int size) {
        if (size == 1) {
            BinaryTreeNode root = new BinaryTreeNode(node.value);

            if (root != null) {
                root.left = null;
                root.right = null;
            }

            return root;
        }

        final int halfSize = size / 2;

        LinkedNode middleNode = node.get(halfSize);

        BinaryTreeNode root = new BinaryTreeNode(middleNode.value);

        root.left = convert(node, halfSize);

        int remaining = size - halfSize - 1;
        if (remaining <= 0) root.right = null;
        else root.right = convert(middleNode.nextNode, size - halfSize - 1);

        return root;
    }

    static class BinaryTreeNode {
        BinaryTreeNode left, right;
        final int value;

        BinaryTreeNode(int value) {
            this.value = value;
        }

        public void printInOrder() {
            if (left != null) left.printInOrder();
            System.out.print(" " + value + " ");
            if (right != null) right.printInOrder();
        }
    }

    static class LinkedNode {
        LinkedNode previousNode, nextNode;
        final int value;

        LinkedNode(int value) {
            this.value = value;
        }

        int size() {
            return 1 + countPrevious() + countNext();
        }

        private int countNext() {
            int size = 0;
            LinkedNode next = nextNode;
            while (next != null) {
                ++size;
                next = next.nextNode;
            }
            return size;
        }

        private int countPrevious() {
            int size = 0;
            LinkedNode previous = previousNode;
            while (previous != null) {
                ++size;
                previous = previous.previousNode;
            }
            return size;
        }

        public LinkedNode get(int index) {
            LinkedNode next = this;
            for (int i = 0; i < index; ++i) {
                next = next.nextNode;
            }

            return next;
        }
    }

    public static void main(String[] args) {
        LinkedNode linkedNode = new LinkedNode(1);
        linkedNode.previousNode = null;
        linkedNode.nextNode = new LinkedNode(2);
        linkedNode.nextNode.nextNode = new LinkedNode(3);
        linkedNode.nextNode.nextNode.nextNode = new LinkedNode(4);
        linkedNode.nextNode.nextNode.nextNode.nextNode = new LinkedNode(5);
        linkedNode.nextNode.nextNode.nextNode.nextNode.nextNode = new LinkedNode(6);
        linkedNode.nextNode.nextNode.nextNode.nextNode.nextNode.nextNode = new LinkedNode(7);
        linkedNode.nextNode.nextNode.nextNode.nextNode.nextNode.nextNode.nextNode = new LinkedNode(8);

        BinarySearchTreeConverter converter = new BinarySearchTreeConverter();
        BinaryTreeNode binaryTreeNode = converter.convert(linkedNode, linkedNode.size());

        binaryTreeNode.printInOrder();
    }

}
