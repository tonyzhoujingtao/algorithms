package tony.zhou.challenge;

/**
 * Created by tonyzhou on 18/1/15.
 * <p/>
 * <p/>
 * You are given a String of number characters (S), like the following for example:
 * <p/>
 * "132493820173849029382910382"
 * <p/>
 * Now, let's assume we tie letters to numbers in order such that:
 * <p/>
 * A = "0"
 * B = "1"
 * C = "2"
 * ...
 * M = "12"
 * N = "13"
 * ...
 * Y = "24"
 * Z = "25"
 * <p/>
 * Write an algorithm to determine how many strings of letters we can make with S, by converting from numbers to letters.
 */
public class AlphabetStringCounter {
    private static final int UNDECIDED = -1;

    private final String numberString;
    private final int[] count;

    public AlphabetStringCounter(String numberString) {
        this.numberString = numberString;
        this.count = new int[numberString.length()];

        count[0] = 1;
        for (int i = 1; i < count.length; i++) {
            count[i] = UNDECIDED;
        }
    }


    public int count() {
        return count(numberString.length() - 1);
    }

    private int count(int index) {
        if (index < 0) {
            return 1;
        }

        if (count[index] != UNDECIDED) {
            return count[index];
        }

        count[index] = count(index - 1) + tryCount(index - 1, index);

        return count[index];
    }

    private int tryCount(int startIndex, int endIndex) {
        if (startIndex >= 0) {
            int number = Integer.parseInt(numberString.substring(startIndex, endIndex + 1));

            if (number <= 25 && number >= 0) {
                return count(startIndex - 1);
            }
        }

        return 0;
    }

    public static void main(String[] args) {
        AlphabetStringCounter counter = new AlphabetStringCounter("132493820173849029382910382298984979758932759238793274923874923742396565918223273286479283982191865026342343");
        System.out.println(counter.count());
    }
}
