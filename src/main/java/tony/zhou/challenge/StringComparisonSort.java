package tony.zhou.challenge;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by tonyzhou on 1/2/15.
 * <p/>
 * Output peek N positive integer in string comparison order. For example, let's say N=1000, then you need to output in string comparison order as below:
 * 1, 10, 100, 1000, 101, 102, ... 109, 11, 110, ...
 */
public class StringComparisonSort {

    private final int n;
    private final List<Integer> sortedSequence;

    public StringComparisonSort(int n) {
        this.n = n;
        sortedSequence = new ArrayList<>(n);
    }

    public static void main(String[] args) {
        StringComparisonSort sort = new StringComparisonSort(100);
        sort.sort();

        System.out.println(sort.getSortedSequence());
    }


    public void sort() {
        List<String> sortedStrings = initializeSortedStrings();

        Collections.sort(sortedStrings);

        populateSortedSequence(sortedStrings);
    }

    public List<Integer> getSortedSequence() {
        return sortedSequence;
    }

    private List<String> initializeSortedStrings() {
        List<String> sortedStrings = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            sortedStrings.add(String.valueOf(i));
        }
        return sortedStrings;
    }

    private void populateSortedSequence(List<String> sortedStrings) {
        for (String string : sortedStrings) {
            sortedSequence.add(Integer.parseInt(string));
        }
    }
}
