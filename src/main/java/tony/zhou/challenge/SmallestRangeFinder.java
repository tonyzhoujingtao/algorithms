package tony.zhou.challenge;

import java.util.*;

import static java.util.Collections.sort;

/**
 * Created by tonyzhou on 26/1/15.
 * <p/>
 * You have k lists of sorted integers. Find the smallest range that includes at least one number from each of the k lists.
 * <p/>
 * For example,
 * List 1: [4, 10, 15, 24, 26]
 * List 2: [0, 9, 12, 20]
 * List 3: [5, 18, 22, 30]
 * <p/>
 * The smallest range here would be [20, 24] as it contains 24 from list 1, 20 from list 2, and 22 from list 3.
 */
public class SmallestRangeFinder {

    private final List<GroupedValue> sortedGroupValues;
    private final Set<Integer> groups;

    public SmallestRangeFinder(List<List<Integer>> sortedIntegerGroups) {
        groups = new HashSet<>();
        sortedGroupValues = new ArrayList<>();

        for (int group = 0; group < sortedIntegerGroups.size(); group++) {
            groups.add(group);

            List<Integer> sortedIntegerGroup = sortedIntegerGroups.get(group);
            for (int integer : sortedIntegerGroup) {
                sortedGroupValues.add(new GroupedValue(integer, group));
            }
        }

        sort(sortedGroupValues);
    }

    public List<GroupedValue> find() {
        int smallestRange = Integer.MAX_VALUE;
        List<GroupedValue> smallestRangeGroupedValues = null;

        for (int i = 0; i < sortedGroupValues.size(); i++) {
            Set<Integer> tmpGroups = new HashSet<>(groups);
            List<GroupedValue> tmpRangeGroupedValues = new ArrayList<>();

            GroupedValue starter = sortedGroupValues.get(i);
            tmpRangeGroupedValues.add(starter);
            tmpGroups.remove(starter.group);

            for (int j = i + 1; !tmpGroups.isEmpty() && j < sortedGroupValues.size(); j++) {
                GroupedValue follower = sortedGroupValues.get(j);
                tmpRangeGroupedValues.add(follower);
                tmpGroups.remove(follower.group);
            }

            if(tmpGroups.isEmpty()) {
                int thisRange = tmpRangeGroupedValues.get(tmpRangeGroupedValues.size() - 1).value - tmpRangeGroupedValues.get(0).value;
                if (smallestRange > thisRange) {
                    smallestRange = thisRange;
                    smallestRangeGroupedValues = tmpRangeGroupedValues;
                }
            }
        }

        return smallestRangeGroupedValues;
    }

    static class GroupedValue implements Comparable<GroupedValue> {
        final int value;
        final Integer group;


        GroupedValue(int value, int group) {
            this.value = value;
            this.group = group;
        }

        @Override
        public String toString() {
            return "GroupedValue{" +
                    "value=" + value +
                    ", group=" + group +
                    '}';
        }

        @Override
        public int compareTo(GroupedValue o) {
            return value - o.value;
        }
    }

    public static void main(String[] args) {
        SmallestRangeFinder finder = new SmallestRangeFinder(Arrays.asList(Arrays.asList(4, 10, 15, 24, 26), Arrays.asList(0, 9, 12, 20), Arrays.asList(5, 18, 22, 30)));
        System.out.println(finder.find());
    }
}
