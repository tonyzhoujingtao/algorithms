package tony.zhou.challenge;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tonyzhou on 18/1/15.
 * <p/>
 * <p/>
 * A book contains with pages numbered from 1 - N. Imagine now that you concatenate all page numbers in the book such that you obtain a sequence of numbers which can be represented as a string. You can compute number of occurences 'k' of certain digit 'd' in this string.
 * <p/>
 * For example, let N=12, d=1, hence
 * <p/>
 * s = '123456789101112' => k=5
 * <p/>
 * since digit '1' occure five times in that string.
 * <p/>
 * Problem: Write a method that, given a digit 'd' and number of its occurences 'k', returns a number of pages N. More precisely, return a lower and upper bound of this number N.
 * <p/>
 * Example:
 * input: d=4, k=1;
 * output {4, 13} - the book has 4-14 pages
 * <p/>
 * input d=4 k=0;
 * output {1, 3} - the book has 1-3 pages
 */
public class DigitOccurrenceCounter {
    public static final int DIGITS = 10;

    private static final List<List<Integer>> digitOccurrences = new ArrayList<>(DIGITS);

    static {
        for (int i = 0; i < DIGITS; i++) {
            List<Integer> occurrence = new ArrayList<>();
            occurrence.add(0);

            digitOccurrences.add(occurrence);
        }
    }

    public Range count(int d, int k) {
        List<Integer> occurrences = digitOccurrences.get(d);

        int lower = -1;
        int upper;

        int previous = 0;

        for (int n = 1; ; n++) {
            Integer occurs = getOccurrences(d, occurrences, n);

            if (occurs.intValue() == k) {
                if (lower == -1) {
                    lower = n;
                }
                previous = n;
            } else if (occurs.intValue() > k) {
                upper = previous;
                break;
            }
        }

        return new Range(lower, upper);
    }

    private Integer getOccurrences(int d, List<Integer> occurrences, int n) {
        if (occurrences.size() <= n) {
            Integer occurs = occurrences.get(n - 1) + calculateOccurrences(n, d);
            occurrences.add(occurs);
            return occurs;
        }
        return occurrences.get(n);
    }

    private Integer calculateOccurrences(int n, int d) {
        int occurrence = 0;

        int number = n;

        while (number > 0) {
            int digit = number % 10;
            number = number / 10;

            if (digit == d) {
                ++occurrence;
            }
        }

        return occurrence;
    }

    static class Range {
        private final int lower;
        private final int upper;


        Range(int lower, int upper) {
            this.lower = lower;
            this.upper = upper;
        }

        public String toString() {
            return "Range[" + lower + "," + upper + "]";
        }
    }

    public static void main(String[] args) {
        DigitOccurrenceCounter counter = new DigitOccurrenceCounter();

        System.out.println(counter.count(4, 1));
        System.out.println(counter.count(4, 0));
        System.out.println(counter.count(4, 100));
        System.out.println(counter.count(4, 1000));
        System.out.println(counter.count(4, 999));
        System.out.println(counter.count(4, 1001));
        System.out.println(counter.count(4, 200));
        System.out.println(counter.count(4, 500));
        System.out.println(counter.count(4, 499));
    }
}
