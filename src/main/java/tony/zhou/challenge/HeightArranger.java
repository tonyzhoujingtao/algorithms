package tony.zhou.challenge;

import java.util.*;

/**
 * Created by tonyzhou on 30/1/15.
 * <p/>
 * You are given two array, first array contain integer which represent heights of persons and second array contain how many persons in front of him are standing who are greater than him in term of height and forming a queue. Ex
 * A: 3 2 1
 * B: 0 1 1
 * It means in front of person of height 3 there is no person standing, person of height 2 there is one person in front of him who has greater height then he, similar to person of height 1. Your task to arrange them
 * Ouput should be.
 * 3 1 2
 * Here - 3 is at front, 1 has 3 in front ,2 has 1 and 3 in front.
 */
public class HeightArranger {
    private static final int defaultValue = Integer.MIN_VALUE;

    private final Set<Integer> sortedHeights;
    private final Map<Integer, Integer> heightToOrder;
    private final List<Integer> orderedHeights;

    public HeightArranger(List<Integer> heights, List<Integer> orders) {
        if (heights.size() != orders.size() || heights.isEmpty())
            throw new IllegalArgumentException();

        sortedHeights = new TreeSet<>(heights);

        heightToOrder = new HashMap<>();
        for (int i = 0; i < heights.size(); i++) {
            heightToOrder.put(heights.get(i), orders.get(i));
        }

        orderedHeights = new ArrayList<>();
        for (int i = 0; i < heights.size(); i++) {
            orderedHeights.add(defaultValue);
        }
    }


    public void arrange() {
        for (Integer height : sortedHeights) {
            int order = heightToOrder.get(height);
            int index = caculateIndex(order);

            orderedHeights.set(index, height);
        }
    }

    private int caculateIndex(int order) {
        int index = 0;

        for (; index < orderedHeights.size(); ++index) {
            if (order <= 0 && orderedHeights.get(index) == defaultValue) {
                return index;
            }

            if (orderedHeights.get(index) == defaultValue) {
                --order;
            }
        }


        return index;
    }

    public List<Integer> getOrderedHeights() {
        return orderedHeights;
    }

    public static void main(String[] args) {
        HeightArranger arranger = new HeightArranger(Arrays.asList(1, 2, 3, 4), Arrays.asList(3, 1, 1, 0));

        arranger.arrange();

        System.out.println(arranger.getOrderedHeights());
    }

}
