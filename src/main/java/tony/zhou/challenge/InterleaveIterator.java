package tony.zhou.challenge;

import java.util.*;

public class InterleaveIterator<E> implements Iterator<E> {
    private final Queue<Iterator<E>> iteratorQueue;
    private Iterator<E> currentIterator;

    public InterleaveIterator(Iterator<Iterator<E>> iterators) {
        iteratorQueue = new ArrayDeque<>();
        while (iterators.hasNext()) {
            iteratorQueue.add(iterators.next());
        }

        currentIterator = null;
    }

    @Override
    public boolean hasNext() {
        getNextIterator();

        return currentIterator.hasNext();
    }

    @Override
    public E next() {
        getNextIterator();

        Iterator<E> tmpIterator = currentIterator;

        currentIterator = null;

        return tmpIterator.next();
    }

    private void getNextIterator() {
        if (currentIterator == null) {
            currentIterator = iteratorQueue.poll();
            while (!(currentIterator.hasNext() || iteratorQueue.isEmpty())) {
                currentIterator = iteratorQueue.poll();
            }

            iteratorQueue.add(currentIterator);
        }
    }

    public static void main(String[] args) {
        List<Integer> list1 = Arrays.asList(1, 2, 3);
        List<Integer> list2 = Arrays.asList(14, 15, 16, 17, 18);
        List<Integer> list3 = Arrays.asList(114, 115, 116, 117);
        List<Integer> list4 = Arrays.asList(1114, 1115);


        List<Iterator<Integer>> aggregatedList = Arrays.asList(list1.iterator(), list2.iterator(), list3.iterator(), list4.iterator());

        InterleaveIterator<Integer> iterator = new InterleaveIterator<>(aggregatedList.iterator());

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
