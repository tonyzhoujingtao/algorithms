package tony.zhou.challenge;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by tonyzhou on 31/1/15.
 * <p/>
 * Given a undirected graph with corresponding edges. Find the number of possible triangles?
 * Example:
 * 0 1
 * 2 1
 * 0 2
 * 4 1
 * <p/>
 * Answer:
 * 1
 */
public class TriangleSolver {
    private static final int DEFAULT_ANSWER = 0;

    private final Map<Integer, Set<Integer>> graph;
    private int answer;

    public TriangleSolver(Pair... pairs) {
        this.answer = DEFAULT_ANSWER;
        graph = new HashMap<>();

        initializeGraph(pairs);
    }

    private void initializeGraph(Pair[] pairs) {
        for (Pair pair : pairs) {
            addNeighbourToFirst(pair);
            addNeighbourToSecond(pair);
        }
    }

    private void addNeighbourToSecond(Pair pair) {
        Set<Integer> neighboursOfSecond = graph.get(pair.second);
        if (neighboursOfSecond == null) {
            neighboursOfSecond = new HashSet<>();
        }
        neighboursOfSecond.add(pair.first);

        graph.put(pair.second, neighboursOfSecond);
    }

    private void addNeighbourToFirst(Pair pair) {
        Set<Integer> neighboursOfFirst = graph.get(pair.first);
        if (neighboursOfFirst == null) {
            neighboursOfFirst = new HashSet<>();
        }
        neighboursOfFirst.add(pair.second);

        graph.put(pair.first, neighboursOfFirst);
    }

    public void solve() {
        if (answer == DEFAULT_ANSWER) {
            solveForTheFirstTime();
        }
    }

    private void solveForTheFirstTime() {
        Set<Integer> nodes = graph.keySet();

        for (Integer node: nodes){
            Set<Integer> neighbours = graph.get(node);
            Set<Integer> updatedNeighbours = new HashSet<>(neighbours);

            for (Integer neighbour: neighbours){
                updatedNeighbours.remove(neighbour);

                for (Integer otherNeighbour : updatedNeighbours){
                    if(isConnected(neighbour, otherNeighbour)){
                        answer+=1;
                    }
                }
            }

            graph.put(node, updatedNeighbours);
        }
    }

    private boolean isConnected(Integer node1, Integer node2) {
        Set<Integer> neighbours = graph.get(node1);
        return neighbours.contains(node2);
    }

    public int getAnswer() {
        return answer;
    }

    private static class Pair {
        int first;
        int second;

        Pair(int first, int second) {
            this.first = first;
            this.second = second;
        }
    }

    public static void main(String[] args) {
        TriangleSolver solver = new TriangleSolver(new Pair(0, 1), new Pair(2, 1), new Pair(0, 2), new Pair(4, 1), new Pair(4, 0), new Pair(4, 2));
        solver.solve();
        System.out.println(solver.getAnswer());
    }
}
