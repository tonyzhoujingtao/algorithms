package tony.zhou.challenge;

/**
 * Created by tonyzhou on 4/2/15.
 * <p/>
 * Calculate the power of an integer to the power of another positive integer.
 */
public class PowerCalculator {

    public static void main(String[] args) {
        PowerCalculator calculator = new PowerCalculator();

        System.out.println(calculator.calculate(2, 2));
        System.out.println(calculator.calculate(2, 3));
        System.out.println(calculator.calculate(2, 4));
        System.out.println(calculator.calculate(2, 5));
        System.out.println(calculator.calculate(2, 6));
        System.out.println(calculator.calculate(2, 7));
        System.out.println(calculator.calculate(2, 8));
        System.out.println(calculator.calculate(2, 10));
        System.out.println(calculator.calculate(2, 20));
        System.out.println(calculator.calculate(2, 30));
        System.out.println(calculator.calculate(2, 31));
        System.out.println(calculator.calculate(2, 32));
    }

    public long calculate(long number, int power) {
        if (power == 0)
            return 1;
        if (power == 1)
            return number;

        long result = number * number;
        result *= calculate(result, power / 2 - 1);

        if (power % 2 == 1) {
            result *= number;
        }

        return result;
    }

}
