package tony.zhou.challenge;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by tonyzhou on 25/1/15.
 * <p/>
 * Given a pattern and a string input - find if the string follows the same pattern and return 0 or 1.
 * Examples:
 * 1) Pattern : "abba", input: "redbluebluered" should return 1.
 * 2) Pattern: "aaaa", input: "asdasdasdasd" should return 1.
 * 3) Pattern: "aabb", input: "xyzabcxzyabc" should return 0.
 */
public class PatternRecognizer {
    private final String pattern;
    private final String input;
    
    private final Map<String, String> assignedSubstrings;
    private final Map<String, Integer> assignedIndex;
    
    private final Map<String, Set<String>> characterToSubstringList;

    public PatternRecognizer(String pattern, String input) {
        this.pattern = pattern;
        this.input = input;

        assignedSubstrings = new HashMap<>();
        assignedIndex = new HashMap<>();

        characterToSubstringList = new HashMap<>();

        initializeCharacterToSubstringList(pattern, input);
    }

    private void initializeCharacterToSubstringList(String pattern, String input) {
        Set<String> allSubstringSet = generateAllSubstring(input);

        for (int i = 0; i < pattern.length(); i++)
            characterToSubstringList.put(String.valueOf(pattern.charAt(i)), allSubstringSet);
    }


    boolean recognize() {
        assignedIndex.clear();
        assignedSubstrings.clear();
        return tryRecognize("", 0);
    }

    private boolean tryRecognize(String head, int index) {
        if (index >= pattern.length())
            return false;

        String character = pattern.substring(index, index + 1);

        String assignedSubstring = assignedSubstrings.get(character);
        if (hasAssignedSubstring(assignedSubstring)) {
            if (checkAndRetry(head, character, assignedSubstring, index)) {
                return true;
            } else {
                unassignSubstringIfAtIndex(character, index);
                return false;
            }
        } else {
            Set<String> substringList = characterToSubstringList.get(character);
            for (String substring : substringList) {
                if (checkAndRetry(head, character, substring, index)) {
                    return true;
                }

                unassignSubstringIfAtIndex(character, index);
            }

            return false;
        }

    }

    private boolean hasAssignedSubstring(String assignedSubstring) {
        return assignedSubstring != null && !assignedSubstring.isEmpty();
    }

    private boolean checkAndRetry(String head, String character, String substring, int index) {
        String newHead = head + substring;

        if (input.equals(newHead)) {
            if (isTheEndOfPattern(index)) {
                assignSubstringIfNotAssigned(character, substring, index);

                System.out.printf("Pattern=%s Input=%s%n", pattern, input);
                System.out.println(assignedSubstrings);

                return true;
            } else {
                unassignSubstringIfAtIndex(character, index);
                return false;
            }
        }
        if (input.startsWith(newHead)) {
            assignSubstringIfNotAssigned(character, substring, index);
            if (tryRecognize(newHead, index + 1))
                return true;
        }

        return false;
    }

    private void unassignSubstringIfAtIndex(String character, int index) {
        if (isAtIndex(character, index, assignedIndex)) {
            unassign(character);
        }
    }

    private void unassign(String character) {
        assignedSubstrings.remove(character);
        assignedIndex.remove(character);
    }

    private boolean isAtIndex(String character, int index, Map<String, Integer> assignedIndex) {
        return assignedIndex.get(character) != null && assignedIndex.get(character) == index;
    }

    private void assignSubstringIfNotAssigned(String character, String substring, int index) {
        if (substringIsNotAssigned(character, assignedIndex)) {
            assign(character, substring, index);
        }
    }

    private void assign(String character, String substring, int index) {
        assignedSubstrings.put(character, substring);
        assignedIndex.put(character, index);
    }

    private boolean substringIsNotAssigned(String character, Map<String, Integer> assignedIndex) {
        return assignedIndex.get(character) == null;
    }

    private boolean isTheEndOfPattern(int index) {
        return index == pattern.length() - 1;
    }

    private Set<String> generateAllSubstring(String string) {
        Set<String> substringSet = new HashSet<>();

        for (int beginIndex = 0; beginIndex < string.length(); beginIndex++) {
            for (int endIndex = beginIndex + 1; endIndex <= string.length(); endIndex++) {
                substringSet.add(string.substring(beginIndex, endIndex));
            }
        }

        return substringSet;
    }

    public static void main(String[] args) {
        recognize("abba", "redbluebluered");
        recognize("aabbccaa", "appleapplebananabananacowcowappleapple");
        recognize("AACCCIIAAA", "AmericaAmericaChinaChinaChinaIndiaIndiaAmericaAmericaAmerica");
        recognize("aabbccddde", "ananballballcomputercomputerdoctordoctordoctorelephant");
        recognize("aabbccdddeaabbccdddeaabbccdddeaabbccdddeaabbccdddeaabbccddde", "ananballballcomputercomputerdoctordoctordoctorelephantananballballcomputercomputerdoctordoctordoctorelephantananballballcomputercomputerdoctordoctordoctorelephantananballballcomputercomputerdoctordoctordoctorelephantananballballcomputercomputerdoctordoctordoctorelephantananballballcomputercomputerdoctordoctordoctorelephant");

    }

    private static void recognize(String pattern, String input) {
        PatternRecognizer recognizer = new PatternRecognizer(pattern, input);
        System.out.println(recognizer.recognize());
        System.out.println();
    }
}
