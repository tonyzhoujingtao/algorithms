package tony.zhou.challenge;

/**
 * Created by tonyzhou on 31/1/15.
 * <p/>
 * Given a 2-D matrix represents the room, obstacle and guard like the following (0 is room, B->obstacle, G-> Guard):
 * 0 0 0
 * B G G
 * B 0 0
 * <p/>
 * calculate the steps from a room to nearest Guard and set the matrix, like this
 * 2 1 1
 * B G G
 * B 1 1
 * Write the algorithm, with optimal solution.
 */
public class StepCalculator {

    private final String[][] matrix;

    public StepCalculator(String[][] matrix) {
        this.matrix = matrix;
    }

    public static void main(String[] args) {
        String[][] matrix = {
                {"0", "0", "0", "0", "0"},
                {"B", "B", "G", "0", "0"},
                {"B", "0", "0", "0", "0"},
                {"B", "0", "0", "0", "0"}};

        StepCalculator calculator = new StepCalculator(matrix);

        calculator.calculate();

        String[][] calculatedMatrix = calculator.getCalculatedMatrix();
        for (int row = 0; row < calculatedMatrix.length; row++) {
            for (int col = 0; col < calculatedMatrix[row].length; col++) {
                System.out.printf("%s ", calculatedMatrix[row][col]);
            }
            System.out.println();
        }
    }

    public void calculate() {
        String patternOfInterest = "G";
        int step = 1;
        boolean hasSomethingToFill = true;

        while (hasSomethingToFill) {
            hasSomethingToFill = false;

            for (int row = 0; row < matrix.length; row++) {
                for (int col = 0; col < matrix[row].length; col++) {
                    if (patternOfInterest.equalsIgnoreCase(matrix[row][col])) {
                        hasSomethingToFill = spread(row, col, step) || hasSomethingToFill;
                    }
                }
            }

            patternOfInterest = String.valueOf(step);

            ++step;
        }
    }

    public String[][] getCalculatedMatrix() {
        return matrix;
    }

    private boolean spread(int row, int col, int step) {
        boolean filledLeft = fillIfLegal(row, col - 1, step);
        boolean filledRight = fillIfLegal(row, col + 1, step);
        boolean filledUp = fillIfLegal(row - 1, col, step);
        boolean filledDown = fillIfLegal(row + 1, col, step);

        return filledLeft || filledRight || filledUp || filledDown;
    }

    private boolean fillIfLegal(int row, int col, int step) {
        boolean hasSomethingToFill = false;

        if (row >= 0 && row < matrix.length && col >= 0 && col < matrix[row].length) {
            if ("0".equalsIgnoreCase(matrix[row][col])) {
                matrix[row][col] = String.valueOf(step);
                hasSomethingToFill = true;
            }
        }

        return hasSomethingToFill;
    }
}
