package tony.zhou.challenge;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

/**
 * Created by tonyzhou on 30/1/15.
 * <p/>
 * Give you an array which has n integers,it has both positive and negative integers.Now you need sort this array in a special way.After that,the negative integers should in the front,and the positive integers should in the back.Also the relative position should not be changed.
 * eg. -1 1 3 -2 2 ans: -1 -2 1 3 2.
 * o(n)time complexity and o(1) space complexity is perfect.
 */
public class NegativePositiveSorter {
    private final List<Integer> integers;

    public NegativePositiveSorter(List<Integer> integers) {
        this.integers = new ArrayList<>(integers);
    }

    public void sort() {
        for (int negativeIndex = 0; negativeIndex < integers.size(); negativeIndex++) {
            if (integers.get(negativeIndex) < 0) {
                bubbleUpNegative(negativeIndex);
            }
        }
    }

    private void bubbleUpNegative(int negativeIndex) {
        for (int index = negativeIndex; index > 0; index--) {
            if (integers.get(index - 1) >= 0) {
                swapValue(index, index - 1);
            } else {
                break;
            }
        }
    }

    private void swapValue(int index1, int index2) {
        Integer tmp = integers.get(index1);
        integers.set(index1, integers.get(index2));
        integers.set(index2, tmp);
    }

    public List<Integer> getSortedList() {
        return new ArrayList<>(integers);
    }


    public static void main(String[] args) {
        NegativePositiveSorter sorter = new NegativePositiveSorter(asList(1, 1, 3, -2, 2, -3));

        sorter.sort();

        System.out.println(sorter.getSortedList());
    }
}
