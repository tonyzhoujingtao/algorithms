package tony.zhou.structure.tree;

import java.util.List;

public abstract class AbstractOrder<T extends Comparable<T>> implements
		TraversalOrder<T> {
	private final List<T> order;

	public AbstractOrder(List<T> order) {
		this.order = order;
	}

	@Override
	public List<T> getOrder() {
		return order;
	}

	@Override
	public T getElement(int index) {
		if (index < order.size())
			return order.get(index);
		return null;
	}

	@Override
	public void removeElement(int index) {
		order.remove(index);
	}

	@Override
	public int findIndex(T value) {
		for (int index = 0; index < order.size(); index++) {
			if (order.get(index).compareTo(value) == 0)
				return index;
		}

		return -1;
	}

	@Override
	public boolean isEmpty() {
		return order.isEmpty();
	}

}
