package tony.zhou.structure.tree;

public class LowestCommonAncestorFinder {
	static Node<Integer> findLowestCommonAncestor(Node<Integer> root,
			int value1, int value2) {
		Node<Integer> emptyTree = null;

		while (root != emptyTree) {
			int value = root.getValue();

			if (value > value1 && value > value2) {
				root = root.getLeft();
			} else if (value < value1 && value < value2) {
				root = root.getRight();
			} else {
				return root;
			}
		}

		return emptyTree;
	}
}
