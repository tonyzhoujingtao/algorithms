package tony.zhou.structure.tree;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Heaps {
	public static Node<Integer> heapifyBinaryTree(Node<Integer> root) {
		List<Node<Integer>> nodes = loadNodes(root);

		Collections.sort(nodes);

		reassignChildrenForEachNode(nodes);

		return nodes.get(0);
	}

	private static void reassignChildrenForEachNode(List<Node<Integer>> nodes) {
		for (int i = 0; i < nodes.size(); i++) {
			int left = 2 * i + 1;
			int right = left + 1;
			nodes.get(i).setLeft(left >= nodes.size() ? null : nodes.get(left));
			nodes.get(i).setRight(
					right >= nodes.size() ? null : nodes.get(right));
		}
	}

	public static int countNodes(Node<Integer> root) {
		if (root == null)
			return 0;

		return 1 + countNodes(root.getLeft()) + countNodes(root.getRight());
	}

	public static List<Node<Integer>> loadNodes(Node<Integer> node) {
		List<Node<Integer>> nodes = new ArrayList<>();

		if (node != null) {
			nodes.add(node);

			nodes.addAll(loadNodes(node.getLeft()));
			nodes.addAll(loadNodes(node.getRight()));
		}

		return nodes;
	}
}
