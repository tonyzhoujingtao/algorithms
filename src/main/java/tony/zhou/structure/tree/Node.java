package tony.zhou.structure.tree;

public class Node<T extends Comparable<T>> implements Comparable<Node<T>> {
	private Node<T> left;
	private Node<T> right;
	private T value;

	public Node(Node<T> left, Node<T> right, T value) {
		this.left = left;
		this.right = right;
		this.value = value;
	}

	public Node() {
	}

	public static <T extends Comparable<T>> int treeHeight(Node<T> n) {
		if (n == null)
			return 0;
		return 1 + Math.max(treeHeight(n.getLeft()), treeHeight(n.getRight()));
	}

	public Node<T> getLeft() {
		return left;
	}

	public void setLeft(Node<T> left) {
		this.left = left;
	}

	public Node<T> getRight() {
		return right;
	}

	public void setRight(Node<T> right) {
		this.right = right;
	}

	void preorderTraversal(Node<T> root) {
		if (root == null)
			return;
		root.printValue();
		preorderTraversal(root.getLeft());
		preorderTraversal(root.getRight());
	}

	void inorderTraversal(Node<T> root) {
		if (root == null)
			return;
		inorderTraversal(root.getLeft());
		root.printValue();
		inorderTraversal(root.getRight());
	}

	void postorderTraversal(Node<T> root) {
		if (root == null)
			return;
		postorderTraversal(root.getLeft());
		postorderTraversal(root.getRight());
		root.printValue();
	}

	private void printValue() {
		System.out.println(value);
	}

	@Override
	public int compareTo(Node<T> o) {
		return getValue().compareTo(o.getValue());
	}

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}
}
