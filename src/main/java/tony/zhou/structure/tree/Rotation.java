package tony.zhou.structure.tree;

public class Rotation {
	public static <T extends Comparable<T>> Node<T> rotateRight(Node<T> oldRoot) {
		Node<T> newRoot = oldRoot.getLeft();
		oldRoot.setLeft(newRoot.getRight());
		newRoot.setRight(oldRoot);
		return newRoot;
	}
}
