package tony.zhou.structure.tree;

import java.util.ArrayList;
import java.util.List;

public class InOrders {
	private InOrders() {
	}

	public static <T extends Comparable<T>> InOrder<T> getLeft(
			InOrder<T> inOrder, T value) {
		List<T> left = new ArrayList<T>();

		for (T v : inOrder.getOrder()) {
			if (v.compareTo(value) == 0) {
				break;
			}
			left.add(v);
		}

		return new InOrder<>(left);
	}

	public static <T extends Comparable<T>> InOrder<T> getRight(
			InOrder<T> inOrder, T value) {
		List<T> right = new ArrayList<T>();

		boolean hasReachRightSide = false;
		for (T v : inOrder.getOrder()) {
			if (!hasReachRightSide && v.compareTo(value) == 0)
				hasReachRightSide = true;

			if (hasReachRightSide && v.compareTo(value) != 0)
				right.add(v);
		}

		return new InOrder<>(right);
	}
}
