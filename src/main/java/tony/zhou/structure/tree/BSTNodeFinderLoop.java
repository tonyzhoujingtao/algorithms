package tony.zhou.structure.tree;

public class BSTNodeFinderLoop<T extends Comparable<T>> implements
		BSTNodeFinder<T> {

	@Override
	public Node<T> findNode(Node<T> root, T value) {
		while (root != null) {
			T currentValue = root.getValue();
			if (currentValue.compareTo(value) == 0)
				break;
			if (currentValue.compareTo(value) < 0) {
				root = root.getRight();
			} else {
				root = root.getLeft();
			}
		}

		return root;
	}

}
