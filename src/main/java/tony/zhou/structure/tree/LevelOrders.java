package tony.zhou.structure.tree;

import java.util.ArrayList;
import java.util.List;

public class LevelOrders {
	private LevelOrders() {
	}

	public static <T extends Comparable<T>> LevelOrder<T> filter(
			LevelOrder<T> levelOrder, List<T> inclusions) {

		List<T> filteredLevelOrder = new ArrayList<>();

		List<T> currentLevelOrder = levelOrder.getOrder();
		for (T value : currentLevelOrder)
			if (inclusions.contains(value))
				filteredLevelOrder.add(value);

		return new LevelOrder<T>(filteredLevelOrder);
	}

	public static <T extends Comparable<T>> LevelOrder<T> filterOut(
			LevelOrder<T> levelOrder, List<T> exclusions) {

		List<T> filteredOutLevelOrder = new ArrayList<>();

		List<T> currentLevelOrder = levelOrder.getOrder();
		for (T value : currentLevelOrder)
			if (!exclusions.contains(value))
				filteredOutLevelOrder.add(value);

		return new LevelOrder<T>(filteredOutLevelOrder);
	}
}
