package tony.zhou.structure.tree;

import java.util.*;

/**
 * Implement a function to check if a tree is balanced.
 * <p/>
 * For the purposes of this question, a balanced tree is defined to be a tree
 * such that no two leaf nodes differ in distance from the root by more than one.
 */
public class Balancer {
    public static <T> boolean isBalanced(Node<T> root) {
        Set<Node> leaves = new HashSet<>();

        breadthFirstSearch(root, leaves);

        int maxHeight = Integer.MIN_VALUE;
        int minHeight = Integer.MAX_VALUE;
        for (Node leaf : leaves) {
            if (leaf.height > maxHeight) {
                maxHeight = leaf.height;
            }

            if (leaf.height < minHeight) {
                minHeight = leaf.height;
            }
        }

        return (maxHeight - minHeight) <= 1;
    }

    private static void breadthFirstSearch(Node root, Set<Node> leaves) {
        Queue<Node> queue = new ArrayDeque<>();
        queue.add(root);

        while (!queue.isEmpty()) {
            Node node = queue.poll();
            if (node.parent == null) {
                node.height = 0;
            } else {
                node.height = node.parent.height + 1;
            }

            if (node.childrean == null || node.childrean.isEmpty()) {
                leaves.add(node);
            } else {
                queue.addAll(node.childrean);
            }
        }
    }

    public static <T> boolean isBalanced2(Node<T> root) {
        return (maxDepth(root) - minDepth(root)) <= 1;
    }

    private static <T> int maxDepth(Node<T> root) {
        if (root == null) return 0;

        int maxDepth = Integer.MIN_VALUE;
        for (Node c : root.childrean) {
            int cMaxDepth = maxDepth(c);
            if (maxDepth < cMaxDepth) {
                maxDepth = cMaxDepth;
            }
        }

        return maxDepth + 1;
    }

    private static <T> int minDepth(Node<T> root) {
        if (root == null) return 0;

        int minDepth = Integer.MAX_VALUE;
        for (Node c : root.childrean) {
            int cMinDepth = minDepth(c);
            if (minDepth > cMinDepth) {
                minDepth = cMinDepth;
            }
        }

        return minDepth + 1;
    }

    private static class Node<T> {
        T value;
        int height;

        Node<T> parent;
        List<Node<T>> childrean;
    }
}
