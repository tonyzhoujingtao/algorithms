package tony.zhou.structure.tree;

public class TreeFactory<T extends Comparable<T>> {

	public Node<T> make(InOrder<T> inOrder, LevelOrder<T> levelOrder) {
		Node<T> root = new Node<T>();

		if (levelOrder.isEmpty()) {
			return null;
		}

		root.setValue(levelOrder.getElement(0));

		root.setLeft(makeLeftSubTree(inOrder, levelOrder, root));
		root.setRight(makeRightSubTree(inOrder, levelOrder, root));

		return root;
	}

	private Node<T> makeLeftSubTree(InOrder<T> inOrder,
			LevelOrder<T> levelOrder, Node<T> root) {
		InOrder<T> leftInOrder = InOrders.getLeft(inOrder, root.getValue());
		LevelOrder<T> leftLevelOrders = LevelOrders.filter(levelOrder,
				leftInOrder.getOrder());
		Node<T> leftSubTree = make(leftInOrder, leftLevelOrders);
		return leftSubTree;
	}

	private Node<T> makeRightSubTree(InOrder<T> inOrder,
			LevelOrder<T> levelOrder, Node<T> root) {
		InOrder<T> rightInOrder = InOrders.getRight(inOrder, root.getValue());
		LevelOrder<T> rightLevelOrders = LevelOrders.filter(levelOrder,
				rightInOrder.getOrder());
		Node<T> rightSubTree = make(rightInOrder, rightLevelOrders);
		return rightSubTree;
	}
}
