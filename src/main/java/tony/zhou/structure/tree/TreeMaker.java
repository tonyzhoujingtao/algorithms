package tony.zhou.structure.tree;

/**
 * Given a sorted (increasing order) array, write an algorithm to create a binary tree with minimal height.
 */
public class TreeMaker {
    public static <T> Node<T> makeTree(T[] array) {
        return makeTree(array, 0, array.length);
    }

    private static <T> Node<T> makeTree(T[] array, int start, int end) {
        if (start < end) {
            Node<T> node = new Node<>();

            int mid = start + (end - start) / 2;
            node.value = array[mid];
            node.left = makeTree(array, start, mid);
            node.right = makeTree(array, mid + 1, end);

            return node;
        }

        return null;
    }

    public static void main(String[] args) {
        Node<Integer> tree = makeTree(new Integer[]{1, 2, 3, 4});
        System.out.println(tree);
    }

    private static class Node<T> {
        T value;
        Node<T> left;
        Node<T> right;
    }
}
