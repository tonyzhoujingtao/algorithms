package tony.zhou.structure.tree;

public class BSTNodeFinderRecursive<T extends Comparable<T>> implements
		BSTNodeFinder<T> {

	@Override
	public Node<T> findNode(Node<T> root, T value) {
		if (root == null)
			return null;

		T currentValue = root.getValue();
		if (currentValue.compareTo(value) == 0)
			return root;
		if (currentValue.compareTo(value) < 0) {
			return findNode(root.getRight(), value);
		} else {
			return findNode(root.getLeft(), value);
		}
	}
}
