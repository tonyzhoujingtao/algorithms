package tony.zhou.structure.tree;

interface BSTNodeFinder<T extends Comparable<T>> {
	Node<T> findNode(Node<T> root, T value);
}
