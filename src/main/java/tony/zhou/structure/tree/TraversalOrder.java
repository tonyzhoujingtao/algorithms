package tony.zhou.structure.tree;

import java.util.List;

public interface TraversalOrder<T extends Comparable<T>> {
	List<T> getOrder();

	T getElement(int index);

	void removeElement(int index);

	int findIndex(T value);

	boolean isEmpty();
}
