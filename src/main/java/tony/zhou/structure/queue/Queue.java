package tony.zhou.structure.queue;

public interface Queue {
    void add(Object e);

    Object poll();

    Object peek();
}
