package tony.zhou.structure.queue;

public class LinkedQueue implements Queue {
    private Entry head = null;
    private Entry tail = null;

    @Override
    public void add(Object e) {
        if (head == null) {
            head = new Entry(e);
            tail = head;
        } else {
            Entry entry = new Entry(e);

            entry.prev = tail;
            tail.next = entry;
            tail = entry;
        }
    }

    @Override
    public Object poll() {
        Entry entry = head;

        head = entry.next;
        head.prev = null;

        return entry.value;
    }

    @Override
    public Object peek() {
        return head.value;
    }

    static class Entry {
        private Object value;
        private Entry prev;
        private Entry next;

        Entry(Object value) {
            this.value = value;
        }
    }
}
