package tony.zhou.structure.stack;

public interface MinStack<T extends Comparable<T>> extends Stack<T> {
    T min();
}
