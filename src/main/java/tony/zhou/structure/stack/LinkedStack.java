package tony.zhou.structure.stack;

public class LinkedStack<T> implements Stack<T> {
    Entry<T> top = null;

    @Override
    public void push(T value) {
        top = new Entry<>(value, top);
    }

    @Override
    public T pop() {
        Entry<T> drop = top;
        top = drop.next;
        return drop.value;
    }

    @Override
    public T peek() {
        return top.value;
    }

    private static class Entry<T> {
        private final T value;
        private final Entry<T> next;

        private Entry(T value, Entry<T> next) {
            this.value = value;
            this.next = next;
        }
    }
}
