package tony.zhou.structure.stack;

import java.util.Stack;

public class MinStackWrap<T extends Comparable<T>> implements MinStack<T> {
    private final Stack<MinEntry<T>> delegate;
    private T min = null;

    public MinStackWrap() {
        this.delegate = new Stack<>();
    }

    @Override
    public T min() {
        return delegate.peek().min;
    }

    @Override
    public void push(T value) {
        if (min == null || min.compareTo(value) > 0) {
            min = value;
        }

        delegate.push(new MinEntry<>(value, min));
    }

    @Override
    public T pop() {
        return delegate.pop().value;
    }

    @Override
    public T peek() {
        return delegate.peek().value;
    }

    private static class MinEntry<T> {
        private final T value;
        private final T min;

        private MinEntry(T value, T min) {
            this.value = value;
            this.min = min;
        }
    }
}
