package tony.zhou.structure.stack;

public interface Stack<T> {
	void push(T object);

	T pop();

	T peek();
}