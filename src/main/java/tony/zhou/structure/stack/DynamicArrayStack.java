package tony.zhou.structure.stack;

import java.util.Arrays;

public class DynamicArrayStack<T> implements Stack<T> {
	private static final int GROWTH_FACTOR = 2;
	private static final int DEFAULT_SIZE = 10;

	private int currentIndex;
	private Object[] items;

	public DynamicArrayStack() {
		this(DEFAULT_SIZE);
	}

	public DynamicArrayStack(int initialSize) {
		currentIndex = 0;
		items = new Object[initialSize];
	}

	@Override
	public void push(T object) {
		if (currentIndex >= items.length) {
			grow();
		}

		items[currentIndex++] = object;
	}

	private void grow() {
		Object[] tmp = Arrays.copyOf(items, items.length);
		items = new Object[items.length * GROWTH_FACTOR];
		for (int i = 0; i < tmp.length; i++) {
			items[i] = tmp[i];
		}
	}

	@Override
	public T pop() {
		T item = null;
		if (currentIndex > 0) {
			item = (T) items[--currentIndex];
			items[currentIndex] = null;
		}
		return item;
	}

	@Override
	public T peek() {
		if (currentIndex <= 0) {
			return null;
		}
		return (T) items[currentIndex - 1];
	}

}
