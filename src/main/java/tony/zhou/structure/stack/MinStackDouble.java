package tony.zhou.structure.stack;

import java.util.Stack;

public class MinStackDouble<T extends Comparable<T>> implements MinStack<T> {
    private final Stack<T> delegate;
    private final Stack<T> minimums;

    public MinStackDouble() {
        delegate = new Stack<>();
        minimums = new Stack<>();
    }

    @Override
    public void push(T object) {
        delegate.push(object);

        if (minimums.peek().compareTo(object) > 0) {
            minimums.push(object);
        }
    }

    @Override
    public T pop() {
        T e = delegate.pop();

        if (minimums.peek() == e) {
            minimums.pop();
        }

        return e;
    }

    @Override
    public T peek() {
        return delegate.peek();
    }

    @Override
    public T min() {
        return minimums.peek();
    }
}
