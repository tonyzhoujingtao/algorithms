package tony.zhou.structure;

public class Hashtable<K, V> {
	private final Entry<K, V> entries[];

	@SuppressWarnings("unchecked")
	public Hashtable(int size) {
		entries = new Entry[size];
		for (int i = 0; i < entries.length; i++) {
			entries[i] = new Entry<>();
		}
	}

	void put(K key, V value) {
		int hashCode = hash(key);

		Entry<K, V> entry = entries[hashCode];

		while (entry != null) {
			if (key.equals(entry.getKey())) {
				entry.setValue(value);
				break;
			} else {
				if (entry.hasNext()) {
					entry = entry.next();
				} else {
					Entry<K, V> newEntry = new Entry<K, V>(key, value);
					entry.setNext(newEntry);
					break;
				}
			}
		}
	}

	void remove(K key) {
		int hashCode = hash(key);

		Entry<K, V> parent = null;
		Entry<K, V> entry = entries[hashCode];

		while (entry != null) {
			if (key.equals(entry.getKey())) {
				if (parent != null) {
					parent.setNext(entry.next());
				}

				entry = null;

				break;
			} else {
				parent = entry;
				entry = entry.next();
			}
		}
	}

	V get(K key) {
		int hashCode = hash(key);

		Entry<K, V> entry = entries[hashCode];

		while (entry != null) {
			if (key.equals(entry.getKey())) {
				return entry.getValue();
			} else {
				entry = entry.next();
			}
		}
		return null;
	}

	int size() {
		int size = 0;

		for (Entry<K, V> entry : entries) {
			while (entry != null) {
				if (entry.getKey() != null) {
					++size;
				}
				entry = entry.next();
			}
		}

		return size;
	}

	private int hash(K key) {
		return Math.abs(key.hashCode() % entries.length);
	}

	static class Entry<K, V> {
		private K key;
		private V value;
		private Entry<K, V> next;

		Entry() {
			this(null, null);
		}

		Entry(K key, V value) {
			this.key = key;
			this.value = value;
			this.next = null;
		}

		Entry<K, V> next() {
			return next;
		}

		boolean hasNext() {
			return next != null;
		}

		void setNext(Entry<K, V> next) {
			this.next = next;
		}

		K getKey() {
			return key;
		}

		V getValue() {
			return value;
		}

		void setKey(K key) {
			this.key = key;
		}

		void setValue(V value) {
			this.value = value;
		}
	}
}
