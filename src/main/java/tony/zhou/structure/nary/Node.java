package tony.zhou.structure.nary;

import java.util.ArrayList;
import java.util.List;

public class Node<T> {
	private final T value;
	private final List<Node<T>> children;

	public Node(List<Node<T>> children, T value) {
		this.children = new ArrayList<>(children);
		this.value = value;
	}

	public int getChildrenCount() {
		return children.size();
	}

	public Node<T> getChild(int index) {
		return children.get(index);
	}

	public T getValue() {
		return value;
	}
}