package tony.zhou.structure.string;

public class Substring {
    public static boolean isSubstring(char[] string, char[] find) {
        if (string[0] == '\0' && find[0] == '\0') return true;

        for (int i = 0; string[i] != '\0' && i < string.length; ++i) {
            boolean found = true;
            for (int j = 0; find[j] != '\0' && j < find.length; ++j) {
                if (string[i + j] != find[j]) {
                    found = false;
                    break;
                }
            }
            if (found) return true;
        }

        return false;
    }
}
